---
Title: RWX Knowledge Net
Subtitle: Component-Oriented Knowledge Management
---

The World needs a new form of knowledge capture, managements, searching
and sharing without strings attached; a knowledge network built and
supported by knowledge workers, not corporations; one built on explicit
trust that is not dependent on HTML and HTTP but can use it where
available.

## Primary Goals and Motivation

* Meet the personal needs of knowledge workers.
* Quickly capture knowledge as source.
* Standardize knowledge source syntax and structure.
* Build on existing standards (GitHub, GitLab, CommonMark, Pandoc).
* Employ Parse -> AST -> Render design.
* Enable quick queries for specific information.
* Facilitate word maps and full knowledge content analysis.
* Simplify `man`-like `curl` command line knowledge queries.
* Leverage PWA tech to produce Progressive Knowledge Apps.
* Create a light-weight, trust-based, decentralized registry.

## Built For Knowledge Workers

Knowledge workers are at the core of all human progress and yet we
continue to struggle with providing tools and approaches specifically
designed to facilitate an individual knowledge worker's workflow. As the
world continues to suffer from mass ignorance and inexperience those who
possess experience and knowledge have the potential to literally save
the world by sharing it without having it impact their workflow.

## Domain Model

The RWX Knowledge Domain Model is not particularly novel or new. In a
world filled with over-engineered solutions to simple problems the
novelty *is* the simplicity. Nodes of `README.md` files (with a
specific, universal, syntax) are grouped into knowledge Bases that can
connect with or without a network.

* A *Knowledge Base* is a highly-optimized and rendered knowledge
  resource ready for consumption and sharing. It is composed of *Nodes*
  in a graph/tree directory data structure with the root Node containing
  a `README.md` with YAML metadata about the Base; a `LICENSE` file; a
  manifest (`mani`) file with all Node refs and update times sorted in
  reverse chronological, lexicographical order; an optional `json` file
  containing a searchable summary; an optional JSON `dex` file
  containing every word, count, and where each is used; and an optional
  `subs` file containing subscription recommendations (and warnings)
  pointing to other Bases by domain. Bases must have a top-level
  Internet domain to be shared over the Internet. Bases must not contain
  any unlinked files or directories cluttering things up --- including
  hidden content --- nothing but Node knowledge source and rendered
  artifacts are allowed (see [Repo Organization]). In the highly
  exceptional event that a file or two is required (such as Netlify
  `_redirects`) they may be added by file name to the `Ignore` attribute
  of the `BaseRoot` `README.md` YAML metadata.

* Each *Knowledge Node* contains location and meta data connecting it to
  the Base and embeds a *Component* at its core. The default Node type
  is `DataDoc` (see below). Other dynamic types aggregate Node content
  together in different ways (TOC, summaries, etc.)

* Each *Knowledge Component* consists of a directory containing a number
  of optional *Asset* files and a required `README.md` file with a YAML
  header and body of DataDoc Markdown (a limited form of Pandoc Markdown
  that forbids HTML but otherwise extends CommonMark to supports tables,
  KaTeX (Math), strictly specified heading autoidentifiers and
  attributes, smart typography, and more).
  
* Each *Knowledge Component Asset* file within the Component directory
  must be linked from the `README.md`. Orphan Assets are forbidden
  including hidden ones. Assets must therefore be in their final form of
  optimization.

* Among the Asset files are optional *Knowledge Component DATA Asset*
  files that must end with a useful but unspecified suffix. These files
  contain data that is to be rendered in place of the `[DATA Optional
  Name](file.suf?s=|)` link blocks within the DataDoc of the Component's
  `README.md` file. DATA files can be anything for which a renderer is
  provided. After the filename a minimal number of single-character,
  URL-escaped parameters may be included.

* Converting a Node from YAML and DataDoc Markdown source into an HTML
  `index.html` page or any other target rendering artifact (PDF, etc.)
  requires a *Lexer*, *Parser*, *Renderer*, and *Writer*. A *Converter*
  fulfills all of these roles. 
  
* The *Knowledge Lexer* coverts the raw [DataDoc
  Markdown](/datadoc/markdown/) into a series of tokens.

* The *Knowledge Parser* transforms the tokens into an abstract syntax
  tree or *AST* and identifies syntax errors.

* The *Knowledge Renderer* then renders the AST as a single string
  buffer or *Body*.

* The *Knowledge Writer* writes the *Body* to one or more files
  potentially employing the use of a *Template* that is aware of both
  the YAML header days and the string Body.

* A *Knowledge Registry* is a place where creators can register their
  knowledge Bases so that other knowledge workers can discover and
  follow them. Registries are *not* required since Bases can be shared
  by email, social media, messaging --- even in person on portable
  storage media.

## Repo Organization

Knowledge Bases are usually stored in a Git source repository where
things can easily become cluttered. To help immediately distinguish
between the core knowledge base content and everything else the
directory name `BASE` is strongly recommended as a convention (not
specification). The name of the repository itself is strongly
recommended to be `KNOWLEDGE` so that paths resolve to `KNOWLEDGE/BASE`.
Capitalization makes these important directories rise to the top of
otherwise cluttered directory listings.

Everything within `BASE` should strictly comply with the specification
about nothing extra, hidden, or orphan described in the [Domain Model]
section, but anything else in the top level directory of the repo is
completely fine. This allows all the configuration, caching, building,
and other build-related files to be saved along with the database. Build
caches can be added to `.gitignore`. 

## Important Files

A Knowledge Base contains the following files within its root directory
(which is usually named `BASE`):

* `README.md` - root node and cover (required)
* `mani` - refs with seconds by last changed (required)
* `json` - searchable summary in JSON
* `subs` - subscription recommendations and warnings
* `words` - every single word, count, and node containing (JSON)
* `<node>/` - node subdirectories or other subdirectories
* `dex/` - reserved (optional) base index node
* `dex/json` - rendered base index data

***Other content is strongly discouraged.*** This keeps it light and
unfettered from specific renderers (other than HTML). Crawlers know
exactly what files and directories to look for and need not sift through
anything else.

### `README.md`

The `README.md` file within the root knowledge base directory contains
meta information about the knowledge base along with the cover in the
body. 

Since all knowledge bases must have their own IP or domain (subdomains
included) The `README.md` file also signals to crawlers that a given
domain potentially contains a knowledge base, the version of which is in
the YAML data. Testing for `README.md` existence can easily be done
with a simple HTTP `HEAD` request in most cases.

This also means that people can carry their knowledge bases around with
them on mobile devices at conferences and such optionally making them
discoverable over local WIFI or even Bluetooth networks without any
need for an Internet server at all.

### Manifest File (`mani`)

The `mani` manifest file is a required list of all knowledge nodes by
`Ref` and the Unix seconds since epoch of when each node was last
modified *in any way*. (The time stamp is different than the `Updated`
property which is explicitly set by the content creator.) Each line
contains the epoch seconds followed by the `Ref` with exactly one space
between them for the fastest possible parsing.

The manifest must be sorted by most recently changed node and then
lexicographically by `Ref` so that the difference between a previous
`mani` file and the current one can most efficiently be determined by
examining only the beginning `mani` files.

### JSON Summary (`json`)

The `json` file contains a compressed JSON summary of the knowledge base
and all its nodes. It is the `TL;DR` of sorts for the entire knowledge
base containing only the meta data, summary, and section headings from
every node. This file is ideal for high-level searches both local and
remote displacing any dependency on other external search engines.

### Subscription Recommendations (`subs`)

Knowledge bases may optionally contain a `subs` file containing
recommendations pointing to other knowledge bases to follow (`+`) or
ignore (`-`) thereby building dependencies on a living network of
trusted knowledge providers (instead of any centralized search engine).

Each line of the `subs` file contains a plus or minus (`+` or `-`)
followed by one space and then a domain with no schema prefix. Every
shared knowledge Base must be visible to the user and have its own
top-level domain (or subdomain). If a knowledge base is known to be
available at several different domains then the first is considered
priority. 

Only secure transfer protocols are allowed in any qualifying RWX
Knowledge Net application. Currently the following will be attempted in
order:

1. HTTPS/TLS
1. SSH/SCP

Since periodic synchronization and local caching removes the demand for
constant network requests, and since SSH provides an industrial strength
method of providing secure access to private Bases, these sharing
protocols work well within the needs of most all knowledge workers.

Knowledge applications can collect the `subs` files of others in any
number of ways establishing circles of trust extending out by level. For
example, one's own knowledge base(s) are considered root level. The
follows indicated in one's own `sub` file are considered *first* level.
The follows within those knowledge bases' `sub` files are *second*
level, and so on.

Ignores (minuses `-`) provide a way for people to collectively indicate
that a resource should not be trusted. Ultimately the decision to trust
a knowledge base is up to the individual and those whom that individual
trusts.

### Words (`words`)

The `words` file is a JSON file containing every single white-space
separated word in any paragraph or heading of every `README.md` file
with its overall count, count per node, and list of all nodes containing
it. It is updated every time the knowledge base is rendered.

### Node Subdirectories

Although the Knowledge Base is itself a DataDoc Node other Nodes are
usually contained within the root Base directory as well. Subdirectories
of other subdirectories are also common for organizing hundreds, or even
thousands of Nodes. The Node.Ref is actually the path to the Node
beginning from the Knowledge Base directory.

## Registry Domains Reserved

The <https://README.world> and <https://know.sh> domains have been
secured to eventually become a Registry reference implementation. The
first is intended for the graphic interface while the second will be
primarily meant for Lynx and curl access even though they will provide
identical content and functionality. 

## Problem with RSS

RSS is not only mired by problems with compatibility, it suffers from a
fundamental flaw in its design: RSS files must contain everything when
sharing knowledge.

The original designers of RSS failed to consider anything other than a
running feed of information, and complicated that by forcing it all into
a finite and complicated XML file. The result is the disastrously bad
design that has largely failed at this point.

Instead, a better approach is to create a `MANIFEST` containing all the
data within the knowledge base and allow subscribers (via their
applications) to diff the knowledge and arrive at a list of all the
changes. This is a well established and successful design pattern used
regularly in software source management. We are simply treating
knowledge as source files. The design is the same.

## See Also:

* [Development Phases](/dev/)
* [DataDoc Markdown](/datadoc/markdown/)
