# Development Phases

Development is planned to build on subsequent phases each with a minimum
viable product during the process initially dependent on Pandoc but
ultimately replacing it with 100% native [DataDoc
Markdown](/datadoc/markdown/) parsing and rendering libraries.

## Phase One: Content Component Model and Pandoc HTML Rendering

The content component types and domain model must be initially specified
in order to test and evolve the rest of the `kn` tool and RWX Knowledge
Base Specification and reference API in Golang.

The first phase will be a self-contained `kn` executable that simply
uses `pandoc` tuned to match DataDoc Markdown specifications as
everything but the Writer, which will be fulfilled with Go HTML
templates instead of depending on Pandoc's weaker template syntax. This
will immediately provide a useful prototype. (Eventually if demand
exists a full Pandoc Converter with Pandoc template and partials support
can be added.)

Phase one includes the strict specification of the [RWX DataDoc
Markdown](/datadoc/markdown/).

## Phase Two: Searchable Progressive Web App and Command

The second phase focuses on metadata extraction to the build process
providing the `json` file and the progressive front-end JavaScript
service worker enhancements to enable local searching as well as simpler
search capabilities built into the `kn` command line tool itself.

This will include full support for social media "cards" so that
individual components display nicely when linked directly in services
like Twitter, Discord, Facebook and others. When possible a component
could even include some amount of interactive progressive enhancement.

## Phase Three: Registry

The third phase is all about enabling the sharing of knowledge bases
simply by providing a place for users to list their URIs and for the
community to blacklist URIs that contain content that does not match
their summary, description, tags and other published properties.

Blacklisting insures one thing only, that the content is being properly
represented. Blacklisting for *any* other reason will result in
banishment from the community registry. 

No knowledge base URIs will ever be removed --- including those
blacklisted --- except by automatic detection that the knowledge base is
no longer available at the given URI, which if continuous over several
weeks will result in automatic removal. 

It is up to the user to decide what to consider adding to their `sub`
collection. Total users who have added a given collection is also *not*
maintained centrally but can be determined locally to a user adding
several users' knowledge Bases to their collection and then looking at
the `subs` of  *their* `subs` and so on. The emphasis is always on the
end users and content creators, not any centralized service.

## Phase Four: Stand Alone Previewing

Initially the `kn` tool will depend on <https://browsersync.io>, a
NodeJS technology for live previewing. Phase four will focus on removing
this dependency entirely and replacing it with an identically
functioning Go equivalent.

## Phase Five: RWX DataDoc Markdown Go Implementation

While support for Pandoc will always be at the core, a native Pandoc
Light Markdown parser will provide parsing and rendering speeds rivaling
SSGs like Hugo and faster than engines like Goldmark. The Pandoc AST
will remain the fundamental focus of all such efforts in order to
maintain 100% compatibility with the Pandoc Markdown specification.

## Phase Six: knowledge.Base.Fetch(), knowledge.Node.Fetch()

Knowledge bases and nodes can be fetched using *any* Internet protocol
provided they have at least one `Base.Source[]` set to an accessible
URI. Content providers can make multiple sources available or the same
source available via multiple protocols similar to how `git` works.

Fetch should be configured to omit nodes by type --- especially dynamic
types.

One of the most important foundations of the RWX approach is that
knowledge is *not* held hostage by any specific protocol and never will
be. Unforeseen new network protocols perhaps including peer-2-peer
technologies can be added without impacting any of the existing
knowledge infrastructure.

## Phase Seven: Specification Publication

After about a year of solid usage the model will be captured in an
formal specification with proper RFCs for the essential models and
components.
