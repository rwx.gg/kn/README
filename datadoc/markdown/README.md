---
Title: What is RWX DataDoc Markdown?
Also:
- T: Pandoc Markdown Specification
  U: https://pandoc.org/MANUAL.html#pandocs-markdown
---

DataDoc Markdown is currently nothing more than Pandoc Markdown (a
superset of CommonMark) that recognizes one-line `[DATA](file.json)`
blocks as positions to insert rendered data and that *strongly suggests*
a set of conventions for the sake of simplicity and uniformity, a sort
of Pandoc Light Markdown.

## DATA Links

* `[DATA](some.json)`{markdown}
* `[DATA Some Title](some.json)`{markdown}
* `[DATA Some Title](some.json?a=1&b=2)`{markdown}

DATA links are just regular Markdown links that are placed on their own
line (technically a paragraph block). If the first word of the link text
is `DATA` then it is identified as a *DATA Link*. 

If `DATA` is followed by a single space than everything up to the
bracket will be considered the caption for the rendered data.

If the file name is followed by a standard URL query string then they
will be included along with the name when passed to the renderer.

This convention requires no additional syntax to be added to Markdown
and the links work as a fallback when viewing the raw source syntax on
services that do not support `DATA` rendering (GitHub, GitLab, etc.).

DataDoc renderers can implement DATA rendering however they wish but
here's one approach:

1. Scan the source or AST for one-line paragraphs with links beginning
   with `DATA` at the start of a paragraph creating an list.
1. Render each `DATA` file into its own buffer.
1. Render the Pandoc Markdown into a memory buffer.
1. Split the buffer based on each rendered one-line paragraph link.
1. Return a new buffer combining the splits and rendered `DATA`.

## DATA Files

The `DATA` files themselves can be anything for which there is a
renderer. The type of renderer to use is mapped to standardized
suffixes. Most tools should natively support rendering of common
structured data types:

* `json`
* `yaml`,`yml`
* `xml`
* `csv`
* `tsv`,`tab`
* `dsv`

### DATA Scripts

`DATA` files and renderers are not limited to structured data. Since
literally anything can serve as a renderer, `DATA` files can contain
scripts that trigger rendering by something like the `bash` interpreter.
This provides unlimited integration in the true spirit of the UNIX
philosophy.

Obviously, calling a script interpreter as a renderer will have a
significant impact on *that* Node being rendered, but when used
judiciously this flexibility can be invaluable allowing everything from
GraphQL queries to full direct database fetches providing essentially a
frozen aggregate view of all DATA resources.

However, often a better design is to have static structured data files
created by external processes that write into the knowledge Node itself.
This is preferred when the snapshot of the data needs to be saved along
with the other knowledge source.

### Clean Separation {#some}

This provides a way to maintain separate of concerns and keep data in
the structured data files where they belong. This separation allows
external processes to created and update those files on the same
rendering system without incurring the overhead of a direct database
query or HTTP fetch. It is, of course, prudent to ensure that no data
files are being modified during any given render.

## Start Simply

While the current Pandoc Markdown will always fully supported (by using
`pandoc` directly). It can be hard to learn and maintain for beginners.
Consider approaching Pandoc Markdown simply at first so that you can
start quickly and create highly compatible knowledge source that will
work *anywhere* Markdown is used. Then as you need Pandoc's full power
you can *progressively* add its additional syntax. [Here is a quick
overview](https://rwx.gg/lang/md/basic/) that you can complete in about
20 minutes.

